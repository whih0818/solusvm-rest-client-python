__author__ = "JaeKyu Jin"
__credits__ = ["JaeKyu Jin", ]
__license__ = "WTFPL"
__version__ = "1.0.0"
__maintainer__ = "JaeKyu Jin"
__email__ = "jagur@jagur.kr"

import urllib
import urllib2


class SolusVMClient:
    def __init__(self, url, api_key, api_hash):
        self.url = url
        self.api_key = api_key
        self.api_hash = api_hash
        self.default_args = {"key": self.api_key, "hash": self.api_hash}

    def _request(self, **kwargs):
        args = self.default_args.copy()
        for key, value in kwargs.iteritems():
            args[key] = value
        encoded_args = urllib.urlencode(args)
        result = urllib2.urlopen(self.url + '?%s' % encoded_args, data=encoded_args)
        return result.read()

    def info(self):
        return self._request(action='info', hdd='true', bw='true', ipaddr='true', mem='true')


    def reboot(self):
        return self._request(action='reboot')

    def boot(self):
        return self._request(action='boot')

    def shutdown(self):
        return self._request(action='shutdown')